import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./Guard/auth.guard";
import { LogedGuard } from "./Guard/loged.guard";

const routes: Routes = [
	{
		path: "login",
		loadChildren: "./pages/login/login.module#LoginPageModule",
		canActivate: [LogedGuard],
	},
	{
		path: "register",
		loadChildren: "./pages/register/register.module#RegisterPageModule",
		canActivate: [LogedGuard],
	},
	{
		path: "forgot-password",
		loadChildren:
			"./pages/forgot-password/forgot-password.module#ForgotPasswordPageModule",
		canActivate: [LogedGuard],
	},
	{ path: "", redirectTo: "dashboard", pathMatch: "full" },
	{
		path: "dashboard",
		loadChildren: "./pages/home/home.module#HomePageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "notifications",
		loadChildren:
			"./pages/notifications/notifications.module#NotificationsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "perfil/:id",
		loadChildren: "./pages/perfil/perfil.module#PerfilPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "doctors",
		loadChildren: "./pages/doctors/doctors.module#DoctorsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "notify-chats",
		loadChildren:
			"./pages/notify-chats/notify-chats.module#NotifyChatsPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "perfil/editar/:id",
		loadChildren:
			"./pages/editar-mi-perfil/editar-mi-perfil.module#EditarMiPerfilPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "estudios",
		loadChildren:
			"./pages/perfil/asset/estudios/estudios.module#EstudiosPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "trabajos",
		loadChildren:
			"./pages/perfil/asset/trabajos/trabajos.module#TrabajosPageModule",
		canActivate: [AuthGuard],
	},
	{
		path: "maps",
		loadChildren: "./pages/maps/maps.module#MapsPageModule",
		canActivate: [AuthGuard],
	},

	{
		path: "mobile-menu",
		loadChildren:
			"./pages/mobile-menu/mobile-menu.module#MobileMenuPageModule",
	},
	{
		path: "chat",
		loadChildren: "./pages/chat/chat.module#ChatPageModule",
		canActivate: [AuthGuard],
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
