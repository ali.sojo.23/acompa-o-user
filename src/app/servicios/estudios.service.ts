import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable({
	providedIn: "root",
})
export class EstudiosService {
	constructor(private http: HttpClient) {}

	url = environment.urlHost;

	urlEducation: string = this.url + "education/";

	createInfo(data, identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);
		return this.http.post(this.urlEducation, data, { headers: headers });
	}
	getEstudios(identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);
		return this.http.get(this.urlEducation + identificador, {
			headers: headers,
		});
	}
}
