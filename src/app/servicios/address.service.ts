import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
@Injectable({
	providedIn: "root",
})
export class AddressService {
	constructor(private httpClient: HttpClient) {}
	url = environment.urlHost;
	urlCountry: string = this.url + "country";
	urlCity: string = this.url + "province/country/";

	obtenerPaises(token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);

		return this.httpClient.get(this.urlCountry, { headers: headers });
	}

	obtenerCiudades(identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);

		return this.httpClient.get(this.urlCity + identificador, {
			headers: headers,
		});
	}
	verCiudades(identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("Authorization", "Bearer " + token);

		return this.httpClient.get(this.urlCity + "?city=" + identificador, {
			headers: headers,
		});
	}
}
