import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

@Injectable({
	providedIn: "root",
})
export class UsuariosService {
	constructor(private httpClient: HttpClient) {}

	url = environment.urlHost;
	urlUsuarios: string = this.url + "user/";

	obtenerUsuarios(): Observable<any> {
		return this.httpClient.get(this.urlUsuarios);
	}
	mostrarUsuario(identificador, token): Observable<any> {
		let headers = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);

		return this.httpClient.get(this.urlUsuarios + identificador, {
			headers: headers,
		});
	}
	actualizarUsuarios(data, token, identificador): Observable<any> {
		let json = JSON.stringify(data);
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);

		return this.httpClient.post(
			this.urlUsuarios + "edit/" + identificador,
			json,
			{
				headers: headers,
			}
		);
	}
	modificarAvatar(avatar, identificador, token): Observable<any> {
		let json = JSON.stringify({ avatar: avatar });

		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("access-token", token);

		return this.httpClient.post(
			this.urlUsuarios + "avatar/" + identificador,
			json,
			{
				headers: headers,
			}
		);
	}
}
