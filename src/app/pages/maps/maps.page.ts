import {
	Component,
	OnInit,
	AfterViewInit,
	ChangeDetectorRef,
} from "@angular/core";
declare var google: any;
@Component({
	selector: "app-maps",
	templateUrl: "./maps.page.html",
	styleUrls: ["./maps.page.scss"],
})
export class MapsPage implements OnInit, AfterViewInit {
	map: any;
	lat: number;
	long: number;
	address: any;
	marker: any;
	constructor(public chRef: ChangeDetectorRef) {}

	ngAfterViewInit() {
		this.loadMap();
	}
	async loadMap() {
		let latLng = JSON.parse(localStorage.getItem("coordinate"));

		let element = document.getElementById("map");

		let options = {
			zoom: 16,
			center: latLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
		};

		this.map = new google.maps.Map(element, options);
		this.marker = new google.maps.Marker({
			map: this.map,
			position: latLng,
		});
		this.marker.setMap(this.map);
		this.findPlace(latLng);
	}
	findPlace(latLng) {
		let geocoder = new google.maps.Geocoder();

		this.marker.setMap(null);
		this.marker = new google.maps.Marker({
			map: this.map,
			position: latLng,
		});
		this.marker.setMap(this.map);

		geocoder.geocode({ latLng: latLng }, (results, status) => {
			if (status == google.maps.GeocoderStatus.OK) {
				this.address = results[0];
				this.chRef.detectChanges();
			}
		});
	}

	ngOnInit() {}
}
