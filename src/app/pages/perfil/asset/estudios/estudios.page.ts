import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { NavController } from "@ionic/angular";
import { EstudiosService } from "../../../../servicios/estudios.service";

@Component({
	selector: "app-estudios",
	templateUrl: "./estudios.page.html",
	styleUrls: ["./estudios.page.scss"],
})
export class EstudiosPage implements OnInit {
	constructor(
		private __location: Location,
		private __navController: NavController,
		private __estudios: EstudiosService
	) {}

	token: any;
	identificador: any;
	data: any = {
		Institute: "",
		Subject: "",
		Start: "",
		End: "",
		degree: "",
		Grade: "",
		User: "",
	};

	goBack() {
		this.__navController.back();
	}
	info() {
		this.token = localStorage.getItem("tk_init");
		this.identificador = localStorage.getItem("import_data");
		this.data.User = this.identificador;
	}

	saveEstudios() {
		this.info();
		this.__estudios
			.createInfo(this.data, this.identificador, this.token)
			.subscribe(
				(resultado) => {
					this.goBack();
				},
				(error) => {
					console.log(error);
				}
			);
	}

	ngOnInit() {}
}
