import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NavController } from '@ionic/angular';
import { ExperienceService } from '../../../../servicios/experience.service';
import { AddressService } from '../../../../servicios/address.service';

@Component({
  selector: 'app-trabajos',
  templateUrl: './trabajos.page.html',
  styleUrls: ['./trabajos.page.scss'],
})
export class TrabajosPage implements OnInit {

  constructor(private __location: 		Location,
  			  private __navController: 	NavController,
  			  private __experiencia: 	ExperienceService,
  			  private __address:		AddressService) { }

  data:any = {
  	company : '',
	country : '',
	city : '',
	position : '',
	start : '',
	end : ''
  };
  today:any = Date.now() ;
  identificador:any;
  token:any;
  pais:any;
  ciudad:any;
  goBack(){
  	this.__navController.back();
  }
  info(){
  	this.token = localStorage.getItem('tk_init');
  	let user = JSON.parse(localStorage.getItem('import_data'));
  	this.identificador = user.id;
  }

  saveExperiencia(){
  		this.info();
  		this.__experiencia.create(this.data,this.identificador,this.token).subscribe( resultado =>{
  			this.goBack();
  		},
  		error =>{
  			console.log(error)
  		});
  }

  getPais(){
  	this.info();
  	this.__address.obtenerPaises(this.token).subscribe(resultado => {
  		this.pais = resultado;
  	},
  	error =>{
  		console.log(error);
  	})
  }

  getCiudad(identificador){
  	this.__address.obtenerCiudades(identificador,this.token).subscribe(resultado => {
  		this.ciudad = resultado
  	},
  	error =>{
  		console.log(error);
  	})
  }

  ngOnInit() {
  	this.getPais();
  }

}
