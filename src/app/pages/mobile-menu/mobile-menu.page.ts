import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
	selector: "app-mobile-menu",
	templateUrl: "./mobile-menu.page.html",
	styleUrls: ["./mobile-menu.page.scss"],
})
export class MobileMenuPage implements OnInit {
	constructor(private __router: Router) {}

	logout() {
		localStorage.clear();

		this.__router.navigate(["/login"]);
	}

	ngOnInit() {}
}
