import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DoctorsPage } from './doctors.page';

import { EliminarDoctorComponent } from './components/eliminar-doctor/eliminar-doctor.component';

const routes: Routes = [
  {
    path: '',
    component: DoctorsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    
  ],
  declarations: [
    DoctorsPage, 
    EliminarDoctorComponent,
  ]
})
export class DoctorsPageModule {}
