import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "../../servicios/usuarios.service";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../environments/environment";
import { AddressService } from "../../servicios/address.service";
import { Observable } from "rxjs/Observable";

@Component({
	selector: "app-editar-mi-perfil",
	templateUrl: "./editar-mi-perfil.page.html",
	styleUrls: ["./editar-mi-perfil.page.scss"],
})
export class EditarMiPerfilPage implements OnInit {
	constructor(
		private __usuario: UsuariosService,
		private __route: ActivatedRoute,
		private __direccion: AddressService,
		private __ruta: Router,
		private __loader: LoadingController,
		private traductor: TranslateService
	) {
		this.obtenerUsuario();
		this.obtenerPais();
		this.urlSlug();
		this.token();
	}
	url: string = environment.urlHost;
	avatar: any;
	usuario: any;
	identificador: string;
	Token: string;
	informacion: any = {
		birthday: "",
		gender: "",
		country: "",
		state: "",
		postalCode: "",
		phone: "",
	};
	country: any;
	ciudad: any;
	error: any;
	load: any;

	obtenerUsuario() {
		this.urlSlug();
		this.token();
		this.__usuario.mostrarUsuario(this.identificador, this.Token).subscribe(
			(resultado) => {
				console.log(resultado);
				this.usuario = resultado.user;
				if (this.usuario.informacion) {
					this.informacion = {
						birthday: this.usuario.informacion.Birthdate,
						gender: this.usuario.informacion.Gender,
						country: this.usuario.informacion.Country,
						state: this.usuario.informacion.Province,
						address: this.usuario.informacion.Address,
						phone: this.usuario.informacion.Phone,
					};
				}
			},
			(error) => {
				console.error(JSON.stringify(error));
			}
		);
	}
	token() {
		this.Token = localStorage.getItem("tk_init");
	}
	urlSlug() {
		this.identificador = this.__route.snapshot.paramMap.get("id");
	}

	obtenerPais() {
		this.token();
		this.__direccion.obtenerPaises(this.Token).subscribe(
			(resultado) => {
				this.country = resultado.country;
			},
			(error) => {
				console.error(JSON.stringify(error));
			}
		);
	}
	obtenerCiudades(identificador, usuario) {
		if (usuario.informacion === null) {
			this.informacion.state = "";
		}
		this.token();
		this.__direccion.obtenerCiudades(identificador, this.Token).subscribe(
			(resultado) => {
				this.ciudad = resultado.province;
			},
			(error) => {
				console.error(JSON.stringify(error));
			}
		);
	}

	almacenarInformacion() {
		this.token();
		let save: any = {
			usuario: this.usuario,
			informacion: this.informacion,
		};
		this.__usuario
			.actualizarUsuarios(save, this.Token, this.usuario._id)
			.subscribe(
				async (resultado) => {
					this.obtenerUsuario();
					console.log(resultado);
					let info = localStorage.getItem("inf_tk");
					if (info == "undefined") {
						localStorage.setItem("inf_tk", resultado.user._id);
					}
					await this.__ruta.navigate([
						"/perfil",
						this.usuario.Auth._id,
					]);
					window.location.reload();
				},
				(error) => {
					console.log(JSON.stringify(error));
				}
			);
	}
	async cargarImagen(event, usuario) {
		this.load = await this.__loader.create({
			message: "Cargando imagen al servidor. Por favor espere",
		});
		this.token();
		this.load.present();
		let reader: any = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				this.avatar = reader.result.split(",")[1];
				this.usuario.avatar = reader.result.split(",")[1];
				this.__usuario
					.modificarAvatar(this.avatar, usuario, this.Token)
					.subscribe(
						(resultado) => {
							this.load.dismiss();
							console.log(resultado);
							this.obtenerUsuario();
						},
						(error) => {
							this.load.dismiss();
							this.obtenerUsuario();
						}
					);
			};
		}
	}

	setLang() {
		this.traductor.setDefaultLang("en");
		let lang = localStorage.getItem("prefer_lang");

		this.traductor.use(lang);
	}

	ngOnInit() {
		this.setLang();
	}
}
