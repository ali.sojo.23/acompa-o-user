import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	RouterStateSnapshot,
	UrlTree,
	CanActivate,
	Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../servicios/auth.service";
import { Location } from "@angular/common";

@Injectable({
	providedIn: "root",
})
export class LogedGuard implements CanActivate {
	constructor(
		private auth: AuthService,
		private router: Router,
		private location: Location
	) {}
	canActivate() {
		this.auth.verifyToken();
		if (this.auth.isLogged) {
			this.router.navigate(["/"]);
			return false;
		}
		return false;
	}
}
